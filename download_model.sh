#!/usr/bin/env bash
set -e
curl -o model.tar -u $DOTSCIENCE_USERNAME:$DOTSCIENCE_API_KEY $DOTSCIENCE_MODEL_URL
tar xf model.tar -C out/